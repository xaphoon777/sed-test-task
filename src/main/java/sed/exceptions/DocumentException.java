package sed.exceptions;

public class DocumentException extends RuntimeException {
    public DocumentException(String s) {
        super(s);
    }
}
