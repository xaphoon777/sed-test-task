package sed.exceptions;

public class TimeRestrictionException extends DocumentException {
    public TimeRestrictionException(String s) {
        super(s);
    }
}
