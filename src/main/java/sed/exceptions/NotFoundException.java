package sed.exceptions;

public class NotFoundException extends DocumentException {
    public NotFoundException(String s) {
        super(s);
    }
}
