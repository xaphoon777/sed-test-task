package sed.exceptions;

public class MaxConversationsRestrictionException extends DocumentException {
    public MaxConversationsRestrictionException(String s) {
        super(s);
    }
}
