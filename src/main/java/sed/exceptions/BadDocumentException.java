package sed.exceptions;

public class BadDocumentException extends DocumentException {
    public BadDocumentException(String s) {
        super(s);
    }
}
