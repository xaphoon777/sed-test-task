package sed.exceptions;

public class UnauthorizedDeleteException extends DocumentException {
    public UnauthorizedDeleteException(String s) {
        super(s);
    }
}
