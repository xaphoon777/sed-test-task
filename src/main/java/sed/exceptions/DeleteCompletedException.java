package sed.exceptions;

public class DeleteCompletedException extends DocumentException {
    public DeleteCompletedException(String s) {
        super(s);
    }
}
