package sed.exceptions;

public class DeleteSignedException extends DocumentException {
    public DeleteSignedException(String s) {
        super(s);
    }
}
