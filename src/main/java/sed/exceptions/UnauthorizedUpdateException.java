package sed.exceptions;

public class UnauthorizedUpdateException extends DocumentException {
    public UnauthorizedUpdateException(String s) {
        super(s);
    }
}
