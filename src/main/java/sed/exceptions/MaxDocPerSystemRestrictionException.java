package sed.exceptions;

public class MaxDocPerSystemRestrictionException extends DocumentException {
    public MaxDocPerSystemRestrictionException(String s) {
        super(s);
    }
}
