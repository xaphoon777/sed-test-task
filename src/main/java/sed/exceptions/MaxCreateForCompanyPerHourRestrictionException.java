package sed.exceptions;

public class MaxCreateForCompanyPerHourRestrictionException extends DocumentException {
    public MaxCreateForCompanyPerHourRestrictionException(String s) {
        super(s);
    }
}
