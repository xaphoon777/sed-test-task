package sed.exceptions;

public class DocumentExistsException extends DocumentException {
    public DocumentExistsException(String s) {
        super(s);
    }
}
