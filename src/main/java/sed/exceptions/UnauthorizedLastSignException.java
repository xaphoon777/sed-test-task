package sed.exceptions;

public class UnauthorizedLastSignException extends DocumentException {
    public UnauthorizedLastSignException(String s) {
        super(s);
    }
}
