package sed.exceptions;

public class SignCompletedException extends DocumentException {
    public SignCompletedException(String s) {
        super(s);
    }
}
