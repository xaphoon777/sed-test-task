package sed.exceptions;

public class UnauthorizedSignerException extends DocumentException {
    public UnauthorizedSignerException(String s) {
        super(s);
    }
}
