package sed;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import sed.exceptions.BadDocumentException;
import sed.exceptions.DeleteCompletedException;
import sed.exceptions.DeleteSignedException;
import sed.exceptions.DocumentExistsException;
import sed.exceptions.NotFoundException;
import sed.exceptions.SignCompletedException;
import sed.exceptions.UnauthorizedDeleteException;
import sed.exceptions.UnauthorizedLastSignException;
import sed.exceptions.UnauthorizedSignerException;
import sed.exceptions.UnauthorizedUpdateException;
import java.util.HashMap;
import java.util.Map;
import sed.restrictions.IRestriction;
import sed.restrictions.RestrictionType;

public class DocSystem {

    private Map<String, Document> documents;
    private List<IRestriction> restrictions;

    private Clock clock = Clock.systemDefaultZone();

    public void setClock(Clock clock) {
        this.clock = clock;
    }

    public DocSystem() {
        documents = new HashMap<>();
        restrictions = new ArrayList<>();
    }

    public void addRestriction(IRestriction restriction) {
        restrictions.add(restriction);
    }

    private void check(Document document, RestrictionType type) {
        restrictions.forEach(restriction -> {
            if (restriction.getType() == type) restriction.check(document);
        });
    }

    private Document findDoc(String name) {
        Document document = documents.get(name);
        if (document == null) {
            throw new NotFoundException("sed.Document not found: " + name);
        }
        return document;
    }

    public void create(String name, String content, Company fromCompany, Company toCompany) {
        Document document = new Document();
        document.setFromCompany(fromCompany);
        document.setToCompany(toCompany);
        check(document, RestrictionType.MAX_DOC_PER_SYSTEM);
        check(document, RestrictionType.MAX_CREATE_FOR_COMPANY_PER_HOUR);
        check(document, RestrictionType.MAX_CONVERSATIONS);

        createInternal(name, content, fromCompany, toCompany);
    }

    private void createInternal(String name, String content, Company fromCompany, Company toCompany) {

        if (name == null) {
            throw new BadDocumentException("name is null");
        }
        if (fromCompany == null) {
            throw new BadDocumentException("fromCompany is null");
        }
        if (toCompany == null) {
            throw new BadDocumentException("toCompany is null");
        }
        if (fromCompany.equals(toCompany)) {
            throw new BadDocumentException("fromCompany and toCompany should not be the same");
        }

        Document document = documents.get(name);
        if (document != null && document.isCompleted()) {
            throw new DocumentExistsException("document already exists");
        }

        document = new Document();
        document.setName(name);
        document.setCreateDate(LocalDateTime.now(clock));
        document.setContent(content);
        document.setFromCompany(fromCompany);
        document.setToCompany(toCompany);
        document.setCompleted(false);
        document.setSignedByFromCompany(false);
        document.setSignedByToCompany(false);

        documents.put(name, document);
    }

    public void sign(String docName, Company company) {
        Document document = findDoc(docName);
        check(document, RestrictionType.TIME);
        if (document.isCompleted()) {
            throw new SignCompletedException(company.getName());
        }
        if (document.getFromCompany().equals(company)) {
            // first sign
            document.setSignedByFromCompany(true);
            return;
        }
        if (document.getToCompany().equals(company)) {
            // last sign
            if (!document.isSignedByFromCompany()) {
                throw new UnauthorizedLastSignException(company.getName());
            }
            document.setSignedByToCompany(true);
            // complete
            document.setCompleted(true);
            return;
        }
        throw new UnauthorizedSignerException(company.getName());
    }

    public void delete(String docName, Company company) {
        Document document = findDoc(docName);
        if (document.isCompleted()) {
            throw new DeleteCompletedException(company.getName());
        }
        if (!document.getFromCompany().equals(company)) {
            throw new UnauthorizedDeleteException(company.getName());
        }
        if (document.isSignedByFromCompany() || document.isSignedByToCompany()) {
            throw new DeleteSignedException(company.getName());
        }
        documents.remove(docName);
    }

    public void update(String docName, String content, Company company) {
        Document document = findDoc(docName);
        if (document.getFromCompany().equals(company)) {
            // update by owner
            createInternal(docName, content, document.getFromCompany(), document.getToCompany());
            return;
        }
        if (document.getToCompany().equals(company)) {
            // update by receiver
            createInternal(docName, content, document.getToCompany(), document.getFromCompany());
            return;
        }
        throw new UnauthorizedUpdateException(company.getName());
    }

    public int count() {
        return documents.size();
    }

    public Map<String, Document> getDocuments() {
        return documents;
    }
}
