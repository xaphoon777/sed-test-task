package sed.restrictions;

import java.util.Map;
import sed.Company;
import sed.DocSystem;
import sed.Document;
import sed.exceptions.MaxConversationsRestrictionException;

/**
 * запрещено ведение более 2х документооборотов между 2мя компаниями
 * (не важно, кто из компаний создаёт документ.
 * Завершенные документообороты не учитываются)
 */
public class MaxConversationsRestriction implements IRestriction {

    /**
     * Максимальное кол-во документов
     */
    private long maxDocs;
    /**
     * Компания 1 на которую распростроаняется ограничение
     */
    private Company restrictedCompany1;
    /**
     * Компания 2 на которую распростроаняется ограничение
     */
    private Company restrictedCompany2;
    private DocSystem docSystem;

    public MaxConversationsRestriction(long maxDocs, Company restrictedCompany1, Company restrictedCompany2, DocSystem docSystem) {
        this.maxDocs = maxDocs;
        this.restrictedCompany1 = restrictedCompany1;
        this.restrictedCompany2 = restrictedCompany2;
        this.docSystem = docSystem;
    }

    @Override
    public RestrictionType getType() {
        return RestrictionType.MAX_CONVERSATIONS;
    }

    @Override
    public void check(Document document) {

        if (!isConversation(document)) {
            return;
        }

        Map<String, Document> documents = docSystem.getDocuments();
        long count = documents
                .entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .filter(this::isConversation)
                .count();
        if (count >= maxDocs) {
            throw new MaxConversationsRestrictionException("Max conversations for two companies violation");
        }
    }

    private boolean isConversation(Document document) {
        Company fromCompany = document.getFromCompany();
        Company toCompany = document.getToCompany();

        return (((fromCompany.equals(restrictedCompany1) && toCompany.equals(restrictedCompany2))
                || fromCompany.equals(restrictedCompany2) && toCompany.equals(restrictedCompany1))
                && (!document.isCompleted()));
    }
}
