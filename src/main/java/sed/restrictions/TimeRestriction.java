package sed.restrictions;

import java.time.Clock;
import java.time.LocalTime;
import sed.Document;
import sed.exceptions.TimeRestrictionException;

/**
 * Запрещено подписывать или вносить изменения в документ с 21:00 до 7:00 утра (создавать документы можно ВСЕГДА)
 */
public class TimeRestriction implements IRestriction {

    private Clock clock = Clock.systemDefaultZone();

    public void setClock(Clock clock) {
        this.clock = clock;
    }

    /**
     * Начало рабочего дня
     */
    private LocalTime workTimeBegin;
    /**
     * Конец рабочего дня
     */
    private LocalTime workTimeEnd;

    public TimeRestriction(LocalTime workTimeBegin, LocalTime workTimeEnd) {
        this.workTimeBegin = workTimeBegin;
        this.workTimeEnd = workTimeEnd;
    }

    @Override
    public RestrictionType getType() {
        return RestrictionType.TIME;
    }

    @Override
    public void check(Document document) {
        LocalTime now = LocalTime.now(clock);
        if (!(now.isAfter(workTimeBegin) && now.isBefore(workTimeEnd))) {
            throw new TimeRestrictionException("Working hours restriction violation");
        }
    }
}
