package sed.restrictions;

import java.util.Map;
import sed.DocSystem;
import sed.Document;
import sed.exceptions.MaxDocPerSystemRestrictionException;

/**
 * Компаниям запрещено участвовать более чем в 10 документооборотах (завершенные не учитываются)
 */
public class MaxDocPerSystemRestriction implements IRestriction {

    /**
     * Максимальное кол-во документов для
     */
    private long maxDocs;

    private DocSystem docSystem;

    public MaxDocPerSystemRestriction(int maxDocs, DocSystem docSystem) {
        this.maxDocs = maxDocs;
        this.docSystem = docSystem;
    }

    @Override
    public RestrictionType getType() {
        return RestrictionType.MAX_DOC_PER_SYSTEM;
    }

    @Override
    public void check(Document document) {
        Map<String, Document> documents = docSystem.getDocuments();
        long count = documents.entrySet().stream().map(Map.Entry::getValue).filter(doc -> !doc.isCompleted()).count();
        if (count >= maxDocs) {
            throw new MaxDocPerSystemRestrictionException("Max open documents per system violation");
        }
    }
}
