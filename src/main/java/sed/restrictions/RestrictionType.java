package sed.restrictions;

public enum RestrictionType {
    TIME,
    MAX_DOC_PER_SYSTEM,
    MAX_CREATE_FOR_COMPANY_PER_HOUR,
    MAX_CONVERSATIONS
}
