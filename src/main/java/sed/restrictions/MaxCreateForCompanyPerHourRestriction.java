package sed.restrictions;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import sed.Company;
import sed.DocSystem;
import sed.Document;
import sed.exceptions.MaxCreateForCompanyPerHourRestrictionException;

/**
 * Компании запрещено создавать более 5 документов в течении 1 часа (внесение изменений не учитывается)
 */
public class MaxCreateForCompanyPerHourRestriction implements IRestriction {

    private Clock clock = Clock.systemDefaultZone();

    public void setClock(Clock clock) {
        this.clock = clock;
    }

    /**
     * Максимальное кол-во документов для компании которые она может создать в промежуток времени
     */
    private long maxDocs;
    /**
     * Промежуток времени в часах
     */
    private int hours;
    /**
     * Компания на которую распростроаняется ограничение
     */
    private Company restrictedCompany;
    private DocSystem docSystem;

    public MaxCreateForCompanyPerHourRestriction(int maxDocs, int hours, Company company, DocSystem docSystem) {
        this.maxDocs = maxDocs;
        this.hours = hours;
        this.restrictedCompany = company;
        this.docSystem = docSystem;
    }

    @Override
    public RestrictionType getType() {
        return RestrictionType.MAX_CREATE_FOR_COMPANY_PER_HOUR;
    }

    @Override
    public void check(Document document) {

        Company company = document.getFromCompany();
        if (company != restrictedCompany) {
            return;
        }

        LocalDateTime controlDateTime = LocalDateTime.now(clock).minus(hours, ChronoUnit.HOURS);


        Map<String, Document> documents = docSystem.getDocuments();
        long count = documents
                .entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .filter(doc -> doc.getFromCompany().equals(restrictedCompany) && doc.getCreateDate().isAfter(controlDateTime))
                .count();
        if (count >= maxDocs) {
            throw new MaxCreateForCompanyPerHourRestrictionException("Max documents per period for company violation");
        }
    }
}
