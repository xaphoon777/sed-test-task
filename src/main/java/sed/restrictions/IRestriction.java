package sed.restrictions;


import sed.Document;

public interface IRestriction {
    RestrictionType getType();

    void check(Document document);
}
