package sed;

import java.time.LocalDateTime;

public class Document {

    private String name;
    private String content;
    private LocalDateTime createDate;
    private Company fromCompany;
    private Company toCompany;
    private boolean signedByFromCompany;
    private boolean signedByToCompany;
    private boolean completed;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public Company getFromCompany() {
        return fromCompany;
    }

    public void setFromCompany(Company fromCompany) {
        this.fromCompany = fromCompany;
    }

    public Company getToCompany() {
        return toCompany;
    }

    public void setToCompany(Company toCompany) {
        this.toCompany = toCompany;
    }

    public boolean isSignedByFromCompany() {
        return signedByFromCompany;
    }

    public void setSignedByFromCompany(boolean signedByFromCompany) {
        this.signedByFromCompany = signedByFromCompany;
    }

    public boolean isSignedByToCompany() {
        return signedByToCompany;
    }

    public void setSignedByToCompany(boolean signedByToCompany) {
        this.signedByToCompany = signedByToCompany;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}
