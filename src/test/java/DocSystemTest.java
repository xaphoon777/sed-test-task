import java.time.Clock;
import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;
import sed.DocSystem;
import sed.exceptions.DeleteCompletedException;
import sed.exceptions.DeleteSignedException;
import sed.exceptions.DocumentException;
import sed.Company;
import sed.exceptions.DocumentExistsException;
import sed.exceptions.MaxConversationsRestrictionException;
import sed.exceptions.MaxCreateForCompanyPerHourRestrictionException;
import sed.exceptions.MaxDocPerSystemRestrictionException;
import sed.exceptions.NotFoundException;
import sed.exceptions.SignCompletedException;
import sed.exceptions.TimeRestrictionException;
import sed.exceptions.UnauthorizedDeleteException;
import sed.exceptions.UnauthorizedLastSignException;
import sed.exceptions.UnauthorizedSignerException;
import sed.exceptions.UnauthorizedUpdateException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import sed.restrictions.MaxConversationsRestriction;
import sed.restrictions.MaxCreateForCompanyPerHourRestriction;
import sed.restrictions.MaxDocPerSystemRestriction;
import sed.restrictions.TimeRestriction;


public class DocSystemTest {

    private static final String TEST_DOC_NAME = "test doc";
    private DocSystem docSystem;
    private Company company1;
    private Company company2;
    private Company company3;
    // фиксируем время для всех тестов
    Clock clock = Clock.fixed(Instant.parse("2019-08-18T10:00:00.00Z"), ZoneId.systemDefault());

    @Before
    public void before() {
        docSystem = new DocSystem();
        docSystem.setClock(clock);
        company1 = new Company("1");
        company2 = new Company("2");
        company3 = new Company("3");
    }

    @Test
    public void createDocumentTest() {
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        Assert.assertEquals(1, docSystem.count());
    }

    @Test(expected = DocumentException.class)
    public void createBadDocTest() {
        docSystem.create(null, null, null, null);
    }

    @Test(expected = UnauthorizedSignerException.class)
    public void unauthorizedSignAttemptTest() {
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        docSystem.sign(TEST_DOC_NAME, company3);
    }

    @Test
    public void FirstSignTest() {
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        docSystem.sign(TEST_DOC_NAME, company1);
    }

    @Test
    public void LastSignTest() {
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        docSystem.sign(TEST_DOC_NAME, company1);
        docSystem.sign(TEST_DOC_NAME, company2);
    }

    @Test(expected = DocumentExistsException.class)
    public void failToCreateExistingTest() {
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        docSystem.sign(TEST_DOC_NAME, company1);
        docSystem.sign(TEST_DOC_NAME, company2);
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
    }

    @Test(expected = UnauthorizedLastSignException.class)
    public void failToSetLastSignWhenFirsSingAbsentTest() {
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        docSystem.sign(TEST_DOC_NAME, company2);
    }

    @Test(expected = NotFoundException.class)
    public void docNotFoundTest() {
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        docSystem.sign("not existing doc", company3);
    }

    @Test(expected = SignCompletedException.class)
    public void failToSignCompletedTest() {
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        docSystem.sign(TEST_DOC_NAME, company1);
        docSystem.sign(TEST_DOC_NAME, company2);
        docSystem.sign(TEST_DOC_NAME, company2);
    }

    @Test
    public void DeleteTest() {
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        docSystem.delete(TEST_DOC_NAME, company1);
        Assert.assertEquals(0, docSystem.count());
    }

    @Test(expected = DeleteSignedException.class)
    public void failToDeleteSignedTest() {
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        docSystem.sign(TEST_DOC_NAME, company1);
        docSystem.delete(TEST_DOC_NAME, company1);
    }

    @Test(expected = DeleteCompletedException.class)
    public void failToDeleteCompletedTest() {
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        docSystem.sign(TEST_DOC_NAME, company1);
        docSystem.sign(TEST_DOC_NAME, company2);
        docSystem.delete(TEST_DOC_NAME, company2);
    }

    @Test(expected = UnauthorizedDeleteException.class)
    public void failToDeleteUnauthorizedTest() {
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        docSystem.sign(TEST_DOC_NAME, company1);
        docSystem.delete(TEST_DOC_NAME, company3);
    }

    @Test
    public void updateByOwnerTest() {
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        docSystem.update(TEST_DOC_NAME, "new content", company1);
        Assert.assertEquals(1, docSystem.count());
    }

    @Test
    public void updateByReceiverTest() {
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        Assert.assertEquals(1, docSystem.count());
        docSystem.update(TEST_DOC_NAME, "new content", company2);
        Assert.assertEquals(1, docSystem.count());
    }

    @Test(expected = UnauthorizedUpdateException.class)
    public void failToUpdateUnauthorizedTest() {
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        Assert.assertEquals(1, docSystem.count());
        docSystem.update(TEST_DOC_NAME, "new content", company3);
        Assert.assertEquals(1, docSystem.count());
    }


    @Test(expected = TimeRestrictionException.class)
    public void restrictionTimeDenyTest() {
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        // сейчас 10 утра
        TimeRestriction restriction = new TimeRestriction(LocalTime.of(15, 30), LocalTime.of(18, 30));
        restriction.setClock(clock);
        docSystem.addRestriction(restriction);
        docSystem.sign(TEST_DOC_NAME, company1);
    }

    @Test
    public void restrictionTimeAllowTest() {
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        // сейчас 10 утра
        TimeRestriction restriction = new TimeRestriction(LocalTime.of(9, 30), LocalTime.of(18, 30));
        restriction.setClock(clock);
        docSystem.addRestriction(restriction);
        docSystem.sign(TEST_DOC_NAME, company1);
    }

    @Test
    public void restrictionMaxDocsAllowTest() {
        MaxDocPerSystemRestriction restriction = new MaxDocPerSystemRestriction(2, docSystem);
        docSystem.addRestriction(restriction);
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        docSystem.create("doc2", "test content", company1, company2);
    }

    @Test(expected = MaxDocPerSystemRestrictionException.class)
    public void restrictionMaxDocsDenyTest() {
        MaxDocPerSystemRestriction restriction = new MaxDocPerSystemRestriction(3, docSystem);
        docSystem.addRestriction(restriction);
        // создаем 1-й
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        // создаем 2-й
        docSystem.create("doc2", "test content", company1, company2);
        // создаем 3-й
        docSystem.create("doc3", "test content", company1, company2);
        // создаем 4-й .. и тут падаем
        docSystem.create("doc4", "test content", company1, company2);
    }

    @Test
    public void restrictionMaxDocsForCompanyAllowTest() {
        MaxCreateForCompanyPerHourRestriction restriction = new MaxCreateForCompanyPerHourRestriction(2, 1, company1, docSystem);
        restriction.setClock(clock);
        docSystem.addRestriction(restriction);
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        docSystem.create("doc2", "test content", company1, company2);
    }

    @Test(expected = MaxCreateForCompanyPerHourRestrictionException.class)
    public void restrictionMaxDocsForCompanyDenyTest() {
        MaxCreateForCompanyPerHourRestriction restriction = new MaxCreateForCompanyPerHourRestriction(1, 1, company1, docSystem);
        restriction.setClock(clock);
        docSystem.addRestriction(restriction);
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        docSystem.create("doc2", "test content", company1, company2);
    }

    @Test
    public void restrictionMaxDocsForCompanyAllowWithUpdatesTest() {
        MaxCreateForCompanyPerHourRestriction restriction = new MaxCreateForCompanyPerHourRestriction(2, 1, company1, docSystem);
        restriction.setClock(clock);
        docSystem.addRestriction(restriction);
        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        docSystem.create("doc2", "test content", company1, company2);
        docSystem.update(TEST_DOC_NAME, "revision 1", company1);
        docSystem.update(TEST_DOC_NAME, "revision 2", company2);
        docSystem.update(TEST_DOC_NAME, "revision 3", company1);
    }

    @Test
    public void restrictionMaxConversationsAllowTest() {
        MaxConversationsRestriction restriction12 = new MaxConversationsRestriction(2, company1, company2, docSystem);
        docSystem.addRestriction(restriction12);

        MaxConversationsRestriction restriction23 = new MaxConversationsRestriction(3, company2, company3, docSystem);
        docSystem.addRestriction(restriction23);

        // Документов много, но диалогов между двумя компаниями в пределах ограничения
        docSystem.create("doc12_1", "test content", company1, company2);
        docSystem.create("doc12_2", "test content", company1, company2);

        docSystem.create("doc23_1", "test content", company2, company3);
        docSystem.create("doc23_2", "test content", company2, company3);
        docSystem.create("doc23_3", "test content", company3, company2);

        docSystem.create("doc13_1", "test content", company1, company3);
        docSystem.create("doc13_2", "test content", company1, company3);
        docSystem.create("doc13_3", "test content", company1, company3);
        docSystem.create("doc13_4", "test content", company1, company3);
    }

    @Test(expected = MaxConversationsRestrictionException.class)
    public void restrictionMaxConversationsDenyTest1() {
        MaxConversationsRestriction restriction12 = new MaxConversationsRestriction(1, company1, company2, docSystem);
        docSystem.addRestriction(restriction12);

        MaxConversationsRestriction restriction23 = new MaxConversationsRestriction(3, company2, company3, docSystem);
        docSystem.addRestriction(restriction23);

        // Документов много, но диалогов между двумя компаниями в пределах ограничения
        docSystem.create("doc12_1", "test content", company1, company2);
        docSystem.create("doc12_2", "test content", company1, company2);

        docSystem.create("doc23_1", "test content", company2, company3);
        docSystem.create("doc23_2", "test content", company2, company3);
        docSystem.create("doc23_3", "test content", company3, company2);
    }

    @Test(expected = MaxConversationsRestrictionException.class)
    public void restrictionMaxConversationsDenyTest2() {
        MaxConversationsRestriction restriction12 = new MaxConversationsRestriction(2, company1, company2, docSystem);
        docSystem.addRestriction(restriction12);

        MaxConversationsRestriction restriction23 = new MaxConversationsRestriction(2, company2, company3, docSystem);
        docSystem.addRestriction(restriction23);

        // Документов много, но диалогов между двумя компаниями в пределах ограничения
        docSystem.create("doc12_1", "test content", company1, company2);
        docSystem.create("doc12_2", "test content", company1, company2);

        docSystem.create("doc23_1", "test content", company2, company3);
        docSystem.create("doc23_2", "test content", company2, company3);
        docSystem.create("doc23_3", "test content", company3, company2);
    }

    @Test
    public void positiveTestWithtwoDifferentRestrictions() {
        MaxConversationsRestriction restriction1 = new MaxConversationsRestriction(2, company1, company2, docSystem);
        MaxDocPerSystemRestriction restriction2 = new MaxDocPerSystemRestriction(10, docSystem);
        docSystem.addRestriction(restriction1);
        docSystem.addRestriction(restriction2);

        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        docSystem.create("doc2", "test content", company1, company2);
    }

    @Test(expected = MaxConversationsRestrictionException.class)
    public void negative1TestWithtwoDifferentRestrictions() {
        MaxConversationsRestriction restriction1 = new MaxConversationsRestriction(2, company1, company2, docSystem);
        MaxDocPerSystemRestriction restriction2 = new MaxDocPerSystemRestriction(10, docSystem);
        docSystem.addRestriction(restriction1);
        docSystem.addRestriction(restriction2);

        docSystem.create(TEST_DOC_NAME, "test content", company1, company2);
        docSystem.create("doc2", "test content", company1, company2);
        docSystem.create("doc3", "test content", company1, company2);
    }

    @Test(expected = MaxDocPerSystemRestrictionException.class)
    public void negativeOneTestWithtwoDifferentRestrictions() {
        MaxConversationsRestriction restriction1 = new MaxConversationsRestriction(2, company1, company2, docSystem);
        MaxDocPerSystemRestriction restriction2 = new MaxDocPerSystemRestriction(3, docSystem);
        docSystem.addRestriction(restriction1);
        docSystem.addRestriction(restriction2);

        docSystem.create("doc32", "test content", company3, company2);
        docSystem.create("doc31", "test content", company3, company1);
        docSystem.create("doc12", "test content", company1, company2);
        docSystem.create("doc21", "test content", company2, company1);
    }


}